/*
Tic tac toe minimax example
*/

class Game {
	constructor(size){
		this.size = size**2;
		this.board = [];

		this.initializeBoard(this.size);
	}

	initializeBoard(size){
		for(var i = 0; i < size; i++){
			this.board.push("-");	
		}
	}

	winningCondition(board){
		var length = Math.sqrt(board) || Math.sqrt(this.size);
		return this.checkRows(length,board) || this.checkColumns(length,board) || this.checkDiagonals(length,board);
	}

	checkRows(length,board){
		var brd = board || this.board
		for(var i = 0; i < length; i++){
			var firstCheck = brd[i*3];
			var found = true;
			for(var j = 1; j < length; j++){
				if(firstCheck != brd[i*3+j] || firstCheck == "-"){
					found = false;
					break;
				}
			}
			if(found){
				return true;
			}	
		}
		return false;
	}

	checkColumns(length,board){
		var brd = board || this.board;
		for(var i = 0; i < length; i++){
			var firstCheck = brd[i];
			var found = true;
			for(var j = 1; j < length; j++){
				if(firstCheck != brd[j*3+i] || firstCheck == "-"){
					found = false;
					break;
				}
			}
			if(found){
				return true;
			}	
		}
		return false;
	}

	checkDiagonals(length,board){
		var brd = board || this.board;
		for(var i = 0; i < 2; i++){
			var firstCheck = brd[i*2];
			var found = true;
			for(var j = 1; j < length; j++){
				if(firstCheck != brd[j*4-(i*2*(j-1))] || firstCheck == "-"){
					found = false;
					break;
				}
			}
			if(found){
				return true;
			}	
		}
		return false;
	}

	placeElement(player,row,column,board){
		var brd = board || this.board;
		var element = player == "Human" ? "X" : "O";
		if(column == undefined && brd[row] == "-"){
			brd[row] = element;
			return true;
		}
		else if(brd[row*3+column] == "-"){
			brd[row*3+column] = element;
			return true;
		}
		return false;
	}

	traceEmpty(board){
		var emptys = [];
		var brd = board || this.board;
		for(var i = 0; i < brd.length; i++){
			if(brd[i] == "-"){
				emptys.push(i);
			}
		}
		return emptys;
	}

	displayBoard(){
		for(var i = 0; i < Math.sqrt(this.board.length); i ++){
			console.log(this.board[i*3] + " " + this.board[i*3+1] + " " + this.board[i*3+2]);
		}
	}
}

class AI{

	constructor(game){
		this.game = game;
	}
	
	setBoard(board){
		this.board = board.slice(0);
	}

	minmax(){
		var depth = 2;
		var max = this.maxPlayer(depth,this.board);
		return max[0];
	}

	maxPlayer(depth,board,tile){
		if(depth == 0){
			return [tile, this.evaluationValue(tile)];
		}else if(this.game.winningCondition(board)){
			return [tile, Infinity]
		}

		let tempBoard = board.slice(0);
		var empty = this.game.traceEmpty(board);
		var max = -Infinity;
		var move = -1; 
		for(var i = 0; i < empty.length; i++){
			let copyArr = board.slice(0);
			this.game.placeElement("AI",Math.floor(empty[i]/3),empty[i]%3,copyArr);
			var min = this.minPlayer(depth-1,board,empty[i]);
			if(max < min[1]){
				move = empty[i];
				max = min[1];
			}
		}
		return [move,max];
	}

	minPlayer(depth,board,tile){
		if(depth == 0){
			return [tile, this.evaluationValue(tile)];
		}else if(this.game.winningCondition(board)){
			return [tile, -Infinity]
		}

		let tempBoard = board.slice(0);
		var empty = this.game.traceEmpty(board);
		var min = Infinity;
		var move = -1; 

		for(var i = 0; i < empty.length; i++){
			let copyArr = board.slice(0);
			this.game.placeElement("Human",Math.floor(empty[i]/3),empty[i]%3,copyArr);
			var max = this.maxPlayer(depth-1,board,empty[i]);
			if(min > max[1]){
				move = empty[i];
				min = max[1];
			}
		}
		return [move, min];
	}

	evaluationValue(tile){
		return this.checkRow(tile) + this.checkColumn(tile) + this.checkDiagonal(tile);
	}

	checkRow(tile){
		var lowestBound = Math.floor(tile/3);
		var highestBound = (lowestBound+1)*3-1;
		var count = 0;
		for(var i = lowestBound; i < highestBound; i++){
			if(this.board[i] == "X"){
				count += 1;
			}
		}
		return count;
	}

	checkColumn(tile){
		var boardLength = Math.sqrt(this.board.length);
		var lowestBound = tile%3;
		var highestBound = boardLength*(boardLength-1)+lowestBound;
		var count = 0;
		for(var i = 0; i < boardLength; i++){
			if(this.board[i*3+lowestBound] == "X"){
				count += 1;
			}
		}
		return count;
	}

	checkDiagonal(tile){
		var count = 0;
		if(tile == 4){
			for(var i = 0; i < 2; i++){
				for(var j = 1; j < length; j++){
					if(this.board[j*4-(i*2*(j-1))] == "X"){
						count += 1;
					}
				}	
			}
		}else{
			var lowestBound = tile%4;
			var highestBound = (this.board.length) - lowestBound;
			var boardLength = Math.sqrt(this.board.length);
			if(lowestBound == 0){
				for(var i = 0; i < boardLength; i ++){
					if(this.board[i*4] == "X"){
						count += 1;
					}
				} 
			}else{
				for(var i = 0; i < boardLength; i ++){
					if(this.board[lowestBound*(i+1)] == "X"){
						count += 1;
					}	
				}
			}
			return count;
		}
	}
}

const game = new Game(3);
const ai = new AI(game);

var turn = Math.random() > 0.5 ? "Human" : "AI"

while(!game.winningCondition() && game.traceEmpty().length > 0){
	var placed = false;
	if(turn != "AI"){
		while(!placed){
			var row = prompt("Masukkan baris :");
			var column = prompt("Masukkan kolom :");
			placed = game.placeElement(turn,parseInt(row-1),parseInt(column-1)) ? true : false;
			game.displayBoard();
		}
	}else{
		ai.setBoard(game.board);
		placed = game.placeElement(turn,ai.minmax()) ? true : false;
		game.displayBoard();
	}
	turn = turn == "Human" ? "AI" : "Human";
}